# README

* Seed 10 users in database with name, email, phone

Consider any one user as logged in user.

1. Display a full page calendar layout(ability to change the month/year)
2. Add dropdown for the list of users
3. Add event to calendar with following fields
  * Title
  * Description
  * users
  * start date and time (datetime picker) [PENDING: Datetimepicker integration]
  * end date and time (datetime picker) [PENDING: Datetimepicker integration]
4. After adding the event, the notification mails should be sent to the users added with accept event invitation links.
5. If Any user selected in the dropdown,  display no. of accepted and not accepted events in the calendar date block (red color count for  not accepted and green for accepted events), and tapping on it should list the events on the date. And from there user should be able to edit/delete the event.
6. If All Users selected in dropdown display only no. of events for the date in the block.
7. Event Creator should be able to edit the event, the new event email should be sent to the new users added and event updated email should be sent to the existing users in the event.
8. When user dropdown is changed, the selected user’s event count should be displayed in the calendar
9. Same user can not be part of multiple event at same time [PENDING]

