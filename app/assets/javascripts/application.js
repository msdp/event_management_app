// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require moment
//= require bootstrap-datetimepicker
//= require pickers
//= require_tree .

$( document ).ready(function() {
    $('#event_start_time').datetimepicker();
    $('#event_end_time').datetimepicker();
});

function select_user(selected_user){
    
  $.ajax({
    type: "GET",
    dataType: "script",
    url: "/events/load_user_events?user_id="+selected_user,
    success: function(data){
        //alert("success");
    }
  }); 
}

function show_events(event_status, event_date){
    ae_id = ".accepted_events_"+event_date 
    re_id = ".rejected_events_"+event_date 
    if (event_status === "accepted"){
      $(ae_id).toggle();
    }
    else{
      $(re_id).toggle();
    }
}