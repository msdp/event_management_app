class ApplicationController < ActionController::Base
    protected  
        def after_sign_in_path_for(resource)
        sign_in_url = new_user_session_url
        if request.referer == sign_in_url
            super
        else
            root_path
        end
        end
end
