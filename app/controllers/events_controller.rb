class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :users_list, only: [:new, :edit]

  def index
    @events = current_user.events
  end

  def new
    @event = Event.new
  end


  def create
    @event = Event.new(event_params)
    if @event.save
      @event.users << User.find(*params[:event_users])
      @event.send_notifications
      redirect_to @event
    else
      render 'new'
    end
  end

  def show
    @event = Event.find(params[:id])
  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])
    if @event.update(event_params)
      existing_users = @event.users.collect(&:id)
      removed_users = existing_users - params[:event_users].map!(&:to_i)
      added_users = params[:event_users] - existing_users
      
      @event.send_update_notifications(existing_users) if !existing_users.empty?
      if !removed_users.empty?
        users = User.find(*removed_users)
        @event.users.delete(User.find(*removed_users))
      end
      @event.users << User.find(*added_users) if !added_users.empty?
      @event.send_notifications(added_users) if !added_users.empty?
      
      redirect_to @event
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to root_path
  end

  def users_list
    @all_users = User.all
  end

  def load_user_events
    if !params[:user_id].blank?
      @request_user = true
      user_id = params[:user_id]
      @user = User.find(user_id)
      @events = @user.events
    else 
      @request_user = false
      @events = Event.all
    end
  end

  private

  def event_params
  	params.require(:event).permit(:event_title, :event_description, :start_time, :end_time, :event_users, :user_id);
  end
end
