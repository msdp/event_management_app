class InvitationsController < ApplicationController
  before_action :find_invitation, only: [:approve_invitation, :reject_invitation]

  def approve_invitation
    @invitation.approve! if (!@invitation.blank? && @invitation.inprocess?)
    redirect_to root_path
  end

  def reject_invitation
    @invitation.approve! if (!@invitation.blank? && @invitation.inprocess?) 
    redirect_to root_path
  end

  def find_invitation
    if params[:event_id].present? && params[:user_id].present?
      @event = Event.find_by_id(params[:event_id])
      @user = User.find_by_id(params[:user_id])
      @invitation = Invitation.where("user_id=? AND event_id=?", @user.id, @event.id).first
    end
  end
end
