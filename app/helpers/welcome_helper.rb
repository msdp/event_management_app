module WelcomeHelper
    def events_accepted date
      if date.blank?
        return 0
      else
       Event.where("start_time=?", date).includes(:users, :invitations).where(invitations: {aasm_state: 'accepted'}).count
      end
    end

    def events_rejected date 
      if date.blank?
       return 0
      else
        Event.where("start_time=?", date).includes(:users, :invitations).where(invitations: {aasm_state: 'rejected'}).count
      end
    end

    def date_id(date)
      date.to_s.gsub("-",'').to_i
    end
end
