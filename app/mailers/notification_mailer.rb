class NotificationMailer < ActionMailer::Base
    default from: 'event-notifications@example.com'
    layout 'mailer'
  
    def event_updated(event, old_event, user)
      @event = event
      @user = user
      @previous_event = old_event
      @pre_event_title = @previous_event['event_title'].last unless @previous_event['event_title'].blank?
      @pre_event_desc = @previous_event['event_description'].last unless @previous_event['event_description'].blank?
      @pre_event_start_time = @previous_event['start_time'].last unless @previous_event['start_time'].blank?
      @pre_event_end_time = @previous_event['end_time'].last unless @previous_event['end_time'].blank?
      mail( :to => @user.email, :subject => 'Reservation Updated' )
    end
  
    def event_success(event, user)
      @event = event
      @user = user
      mail( :to => @user.email, :subject => 'Your Event successfully created' )
    end
  
   
  end