class Event < ApplicationRecord
    
    has_and_belongs_to_many :users
    has_many :invitations

    

    validates_presence_of :event_title,:event_description,:start_time, :end_time
    validate :end_date_is_after_start_date
    #after_save :send_notifications
    #after_update :send_update_notification


    def send_notifications(users_list=[])
      puts "SEND NOTIFICATIONS:::\n"
      if users_list.empty?
        event_users = self.users 
      else
        event_users = User.where("id IN(?)", users_list)
      end
      Rails.logger.info "#{event_users.inspect} \n"
      event_users.each do |user|
        invitation_exists = self.invitations.where("user_id=?", user.id).first
        if invitation_exists.blank?
          invitation = self.invitations.build(event_id: self.id, user_id: user.id, aasm_state: "sent")
          invitation.save
          invitation.submit!
          NotificationMailer.event_success(self, user).deliver
        end
      end     
    end

    def send_update_notifications(users_list=[])
      event_users = User.where("id IN(?)", users_list)
      event_users.each do |user|
        NotificationMailer.event_updated(self, previous_changes, user).deliver
      end 
    end

    def accepted
      invitations.accepted.count
    end

    def rejected
      invitations.rejected.count
    end
    

    private
      def end_date_is_after_start_date
        return if end_time.blank? || start_time.blank?
      
        if end_time < start_time
          errors.add(:end_date, "cannot be before the start date") 
        end 
      end
end
