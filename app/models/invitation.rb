class Invitation < ApplicationRecord
    belongs_to :user
    belongs_to :event

    include AASM

    aasm do
      state :sent, initial: true
      state :inprocess
      state :accepted
      state :rejected
    
      event :submit do
        transitions from: :sent, to: :inprocess
      end
  
      event :approve do
        transitions from: [:inprocess],  to: :accepted
      end
  
      event :reject do
        transitions from: [:inprocess],  to: :rejected
      end
    end
end
