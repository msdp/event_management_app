class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :invitations
     
  
  has_and_belongs_to_many :events


  def accepted_invitations(date)
    events.where("start_time=?", date).includes(:invitations).where(invitations:{aasm_state:"accepted"})
  end
  def rejected_invitations(date)
    events.where("start_time=?", date).includes(:invitations).where(invitations:{aasm_state:"rejected"})
  end
end
