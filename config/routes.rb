Rails.application.routes.default_url_options[:host] = 'localhost:3000'

Rails.application.routes.draw do
  devise_for :users
  resources :events do 
    collection do 
      get :load_user_events
    end
  end
  resources :invitations do 
    collection do 
      get :approve_invitation
      get :reject_invitation
    end
  end
  root "welcome#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
