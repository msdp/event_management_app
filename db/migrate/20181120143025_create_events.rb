class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.references :user, index: true
      t.string :event_title
      t.text :event_description
      t.datetime :start_time
      t.datetime :end_time
      # t.time :start_time
      # t.time :end_time
      t.timestamps
    end
  end
end
