class AddEventUsersToEventTable < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :event_users, :text, array: true, default: []
  end
end
