class CreateInvitations < ActiveRecord::Migration[5.2]
  def change
    create_table :invitations do |t|
      t.references :user, index: true
      t.references :event, index: true
      t.string :aasm_state, index: true
      t.timestamps
    end
  end
end
