# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times.each do |u|
  check_user = User.find_by_email("test_user_#{u}@example.com")
  if check_user.blank?
    user = User.new
    user.email = "test_user_#{u}@example.com"
    user.password = 'test1234'
    user.password_confirmation = 'test1234'
    user.save!
  else 
    puts "#{check_user.email} exits"
  end
end